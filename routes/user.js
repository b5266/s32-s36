const express = require("express");
const router = express.Router();
const userController = require("../controllers/user");
const auth = require ("../auth");

// route checking if the user's email exists in the database
// endpoint: localhost:4000/users/checkEmail
router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
})

// Route for registering a user
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
})

//
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
})

//
router.get("/details", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	// Provides the user's ID for the getProfile controller method

	userController.getProfile({userId : userData.id}).then(resultFromController => res.send(resultFromController));
});

// Route to enroll user to a course
router.post("/enroll", auth.verify, (req, res) => {
	let data = {
		userId : auth.decode(req.body.userId).id,
		courseId : req.body.courseId
	}

	userController.enroll(data).then(resultFromController => res.send(resultFromController));

})

module.exports = router;