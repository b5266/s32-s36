const Course = require("../models/Course");

// Controller function for creating a course
module.exports.addCourse = (data) => {

	// User is admin
	if (data.isAdmin) {

		// Creates a variable "newCourse" and instantiates a new "Course" object using the mongoose model
		// Uses the information from the request body to provide all the necessary information
		let newCourse = new Course({
			name : data.course.name,
			description : data.course.description,
			price : data.course.price
		});

		// Saves the created object to our database
		return newCourse.save().then((course, error) => {

			// Course Creation Successful
			if(error){

				return false;

			// Course creation failed
			} else {

				return true;

			};

		});

		// User is not an admin
	 }
	 	else{
		return Promise.resolve(false);
	};
};

// Controller Function for getting allcourses
module.exports.getAllCourses = () => {
	return Course.find({}).then(result => {
		return result;
	})
}

// Controller function for retrieving a specific course
module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams.courseId).then(result => {
		return result;
	})
}

// Controller function for updating a course
module.exports.updateCourse = (reqParams, reqBody) => {
	// Specify the fileds/properties of the document to be updated
	let updateCourse = {
		name : reqBody.name,
		description : reqBody.description,
		price : reqBody.price
	}

	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((
		course, error) => {
		if(error){
			return false;
		}
		else{
			return true;
		}
	})
}
// Activity
// Controller for Archiving a course
module.exports.archiveCourse = (reqParams) => {
	
	let updateActiveField = {
		isActive : false
	};

	return Course.findByIdAndUpdate(reqParams.courseId, updateActiveField).then((
		course, error) => {
		if(error){
			return false;
		}
		else{
			return true;
		}
	})
}
