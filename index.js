const express = require ("express");
const mongoose = require ("mongoose");
// allows our backend application to be available to our frontend application
const cors = require ("cors");
// allows access to routes defined within the application
const userRoutes = require("./routes/user");
const courseRoutes = require("./routes/course");

const app = express();

// Connect to our MongoDB data base
mongoose.connect("mongodb+srv://admin_zandro:All!sw3ll051591@cluster0.6i70n.mongodb.net/S32-S36?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

// Prompts a message for a successful database connection
mongoose.connection.once('open', () => console.log('Now connected to MongoDB Atlas'));

// allows all resources to access the backend application
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Defines the "/users" string 
app.use("/users", userRoutes);
// Defines the "course" string to be included for all course routes in the "course" route file
app.use("/courses", courseRoutes);

// App listening  to port 4000
app.listen(process.env.PORT || 4000, () => {
	console.log(`API is now online on port ${ process.env.PORT || 4000 }`)
});